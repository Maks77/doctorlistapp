/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/components/Card.js":
/*!********************************!*\
  !*** ./src/components/Card.js ***!
  \********************************/
/*! exports provided: Card */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Card", function() { return Card; });
/* harmony import */ var _helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../helpers/ElementCreator */ "./src/helpers/ElementCreator.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }


var Card =
/*#__PURE__*/
function () {
  function Card() {
    _classCallCheck(this, Card);

    this.card = {}; //elements for update

    this.avatar = '';
    this.title = '';
    this.status = '';
    this.rate = null;
    this.commentsCount = null;
    this.speciality = [];
    this.price = '';
    this.priceTiming = '';
  }

  _createClass(Card, [{
    key: "buildDomElement",
    value: function buildDomElement() {
      var card = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('div', 'card'); //Avatar

      var avatarImg = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('img', 'card__img');
      avatarImg.setAttribute('src', this.avatar);
      avatarImg.setAttribute('alt', 'avatar');
      card.appendChild(avatarImg); //Link

      var link = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('a', 'fas fa-chevron-right card__more');
      link.setAttribute('href', '#');
      card.appendChild(link); //CardTitle

      var cardTitle = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('h3', 'cart__title');
      cardTitle.innerText = this.title;
      card.appendChild(cardTitle); //CardStatus

      var cardStatus = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('div', 'card__status');
      cardStatus.innerText = this.status ? "В сети" : "Не в сети";
      this.status ? cardStatus.classList.add('card__status--online') : cardStatus.classList.add('card__status--offline');
      card.appendChild(cardStatus); //Rating

      var rating = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('div', 'rating');
      var ratingStars = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('div', 'rating__stars');

      for (var i = 0; i < 5; i++) {
        var starClass = this.rate > i ? 'fas fa-star' : 'far fa-star';
        var star = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('i', starClass);
        ratingStars.appendChild(star);
      }

      var ratingCount = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('span', 'rating__count');
      ratingCount.innerText = "(".concat(this.rate, ")");
      var ratingReview = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('span', 'rating__review');
      ratingReview.innerText = "".concat(this.commentsCount, " \u043E\u0442\u0437\u044B\u0432\u043E\u0432");
      rating.appendChild(ratingStars);
      rating.appendChild(ratingCount);
      rating.appendChild(ratingReview);
      card.appendChild(rating); //Card speciality if exists

      if (this.speciality !== undefined) {
        var cardSpeciality = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('div', 'card__speciality');
        cardSpeciality.innerText = this.speciality.join(', ');
        card.appendChild(cardSpeciality);
      } // Buttons


      var buttonHolder = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('div', 'card__button-holder');
      var btnPhone = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('div', 'card__button button-phone');
      var btnVideo = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('div', 'card__button button-video');
      var btnMessage = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_0__["createElement"])('div', 'card__button button-message');
      buttonHolder.appendChild(btnPhone);
      buttonHolder.appendChild(btnVideo);
      buttonHolder.appendChild(btnMessage);
      card.appendChild(buttonHolder);
      this.card = card;
    }
  }, {
    key: "update",
    value: function update(item) {
      this.avatar = item.avatar;
      this.title = item.last_name + item.first_name;
      this.status = item.status;
      this.rate = item.rate;
      this.commentsCount = item.comments;
      this.speciality = item.speciality !== undefined ? item.speciality : [];
      this.price = "250 грн";
      this.priceTiming = "(10 минут)";
      this.buildDomElement();
    }
  }, {
    key: "render",
    value: function render(parent) {
      parent.appendChild(this.card);
    }
  }]);

  return Card;
}();

/***/ }),

/***/ "./src/helpers/ElementCreator.js":
/*!***************************************!*\
  !*** ./src/helpers/ElementCreator.js ***!
  \***************************************/
/*! exports provided: createElement */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createElement", function() { return createElement; });
function createElement(template, className) {
  var elem = document.createElement(template);
  elem.className = className;
  return elem;
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_Card__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/Card */ "./src/components/Card.js");
/* harmony import */ var _helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers/ElementCreator */ "./src/helpers/ElementCreator.js");
/* harmony import */ var _utils_FilterManager__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utils/FilterManager */ "./src/utils/FilterManager.js");
function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }





var data = __webpack_require__(/*! ./user_data.json */ "./src/user_data.json");

var skipBtn = document.querySelector('.skip');
var searchInput = document.querySelector('.search__input');
var filterForm = document.querySelector(".filter__input-holder");
var filterInput = document.querySelector('.filter__input');
var cardHolder = document.querySelector('.card-holder');
var filterBtnContainer = document.querySelector('.filter__button-container');
var mainData = {}; //Add reactivity to mainData object

Object.defineProperties(mainData, {
  'data': {
    get: function get() {
      return this.$data;
    },
    set: function set(value) {
      this.$data = value;
      renderElements(this.$data, this.$nameFilterValue, this.$specFilterArr);
    }
  },
  'nameFilterValue': {
    get: function get() {
      return this.$nameFilterValue;
    },
    set: function set(value) {
      this.$nameFilterValue = value;
      renderElements(this.$data, this.$nameFilterValue, this.$specFilterArr);
    }
  },
  'specFilterArr': {
    get: function get() {
      return this.$specFilterArr;
    },
    set: function set(value) {
      this.$specFilterArr = value;
      renderFilterButtons(this.$specFilterArr);
      renderElements(this.$data, this.$nameFilterValue, this.$specFilterArr);
    }
  }
}); //Method for adding speciality filter

mainData.addSpecFilter = function (value) {
  console.log('addFilter', this);

  if (Array.isArray(this.specFilterArr)) {
    if (this.specFilterArr.includes(value)) {
      return;
    }

    this.specFilterArr = _construct(Array, _toConsumableArray(this.specFilterArr).concat([value]));
  } else {
    this.specFilterArr = [value];
  }
}; //Method for removing separate speciality filter


mainData.removeSpecFilter = function (value) {
  var newArr = _construct(Array, _toConsumableArray(this.specFilterArr));

  newArr = newArr.filter(function (el) {
    return el != value;
  });
  this.specFilterArr = newArr;
}; //Method for removing all filters


mainData.clearFilters = function () {
  this.specFilterArr = new Array();
  this.nameFilterValue = '';
}; //Inflate reactive object with data


mainData.data = data; //Listen for SKIP button click

skipBtn.addEventListener('click', function (e) {
  searchInput.value = null;
  ;
  filterInput.value = null;
  mainData.clearFilters();
}); //Listen for searchInput text change

searchInput.addEventListener('input', function (event) {
  mainData.nameFilterValue = this.value;
}); //Listen for submit of filter form ("Press Enter")

filterForm.addEventListener('submit', function (e) {
  e.preventDefault();
  mainData.addSpecFilter(e.target[0].value);
  e.target[0].value = null;
}); // ================= RENDER ==================
//Render filter buttons

function renderFilterButtons(arr) {
  filterBtnContainer.innerHTML = '';
  arr.forEach(function (el) {
    var btn = Object(_helpers_ElementCreator__WEBPACK_IMPORTED_MODULE_1__["createElement"])('div', 'filter-button');
    btn.innerText = el;
    btn.addEventListener('click', function (e) {
      mainData.removeSpecFilter(e.target.innerText);
    });
    filterBtnContainer.appendChild(btn);
  });
} //Render cards


function renderElements(incomingData, nameFilterValue, specFilterArr) {
  cardHolder.innerHTML = "";
  var data = incomingData;

  if (nameFilterValue != undefined) {
    data = _utils_FilterManager__WEBPACK_IMPORTED_MODULE_2__["FilterManager"].filterByName(data, nameFilterValue);
  }

  if (specFilterArr != undefined) {
    data = _utils_FilterManager__WEBPACK_IMPORTED_MODULE_2__["FilterManager"].filterBySpec(data, specFilterArr);
  }

  data.forEach(function (el) {
    var card = new _components_Card__WEBPACK_IMPORTED_MODULE_0__["Card"]();
    card.update(el);
    card.render(cardHolder);
  });
}

/***/ }),

/***/ "./src/user_data.json":
/*!****************************!*\
  !*** ./src/user_data.json ***!
  \****************************/
/*! exports provided: 0, 1, 2, 3, 4, 5, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"id\":\"02a521fd-f67c-432d-9d75-ad481a691bef\",\"first_name\":\"Bates\",\"last_name\":\"Marquez \",\"status\":true,\"comments\":3,\"rate\":5,\"avatar\":\"http://placehold.it/32x32\",\"speciality\":[\"Хирург\",\"Гастроэнтеролог\",\"Терапевт\"]},{\"id\":\"87f40694-87c6-4ce7-8b3c-abf777c858a3\",\"first_name\":\"Logan\",\"last_name\":\"Osborn \",\"status\":true,\"comments\":5,\"rate\":5,\"avatar\":\"http://placehold.it/32x32\",\"speciality\":[\"Гастроэнтеролог\",\"Терапевт\"]},{\"id\":\"2dd2d55b-c6f5-4cb0-8dfc-e35339462945\",\"first_name\":\"Lydia\",\"last_name\":\"Pollard \",\"status\":false,\"comments\":1,\"rate\":5,\"avatar\":\"http://placehold.it/32x32\",\"speciality\":[]},{\"id\":\"a2e9b160-44ce-4bfa-b120-9b4d52a44071\",\"first_name\":\"Rebekah\",\"last_name\":\"Cote \",\"status\":false,\"comments\":1,\"rate\":5,\"avatar\":\"http://placehold.it/32x32\",\"speciality\":[\"Терапевт\"]},{\"id\":\"43596a12-7a60-4c5f-8d8b-ea581d0c5f29\",\"first_name\":\"Mattie\",\"last_name\":\"Malone \",\"status\":true,\"comments\":7,\"rate\":5,\"avatar\":\"http://placehold.it/32x32\",\"speciality\":[\"Хирург\"]},{\"id\":\"f14cecb8-cb93-487f-85d1-758301b7890c\",\"first_name\":\"Kenya\",\"last_name\":\"Mcdaniel \",\"status\":true,\"comments\":4,\"rate\":5,\"avatar\":\"http://placehold.it/32x32\"}]");

/***/ }),

/***/ "./src/utils/FilterManager.js":
/*!************************************!*\
  !*** ./src/utils/FilterManager.js ***!
  \************************************/
/*! exports provided: FilterManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterManager", function() { return FilterManager; });
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var FilterManager =
/*#__PURE__*/
function () {
  function FilterManager() {
    _classCallCheck(this, FilterManager);
  }

  _createClass(FilterManager, null, [{
    key: "filterByName",
    value: function filterByName(data, value) {
      return data.map(function (el) {
        return _objectSpread({}, el, {
          fullName: (el.last_name + el.first_name).toLowerCase()
        });
      }).filter(function (el) {
        return el.fullName.indexOf(value) != -1;
      });
    }
  }, {
    key: "filterBySpec",
    value: function filterBySpec(data, filters) {
      return data.filter(function (el) {
        return filters.map(function (el) {
          return el.toLowerCase();
        }).every(function (item) {
          if (el.speciality !== undefined) {
            return el.speciality.map(function (el) {
              return el.toLowerCase();
            }).includes(item);
          }
        });
      });
    }
  }]);

  return FilterManager;
}();

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NvbXBvbmVudHMvQ2FyZC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvaGVscGVycy9FbGVtZW50Q3JlYXRvci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3V0aWxzL0ZpbHRlck1hbmFnZXIuanMiXSwibmFtZXMiOlsiQ2FyZCIsImNhcmQiLCJhdmF0YXIiLCJ0aXRsZSIsInN0YXR1cyIsInJhdGUiLCJjb21tZW50c0NvdW50Iiwic3BlY2lhbGl0eSIsInByaWNlIiwicHJpY2VUaW1pbmciLCJjcmVhdGVFbGVtZW50IiwiYXZhdGFySW1nIiwic2V0QXR0cmlidXRlIiwiYXBwZW5kQ2hpbGQiLCJsaW5rIiwiY2FyZFRpdGxlIiwiaW5uZXJUZXh0IiwiY2FyZFN0YXR1cyIsImNsYXNzTGlzdCIsImFkZCIsInJhdGluZyIsInJhdGluZ1N0YXJzIiwiaSIsInN0YXJDbGFzcyIsInN0YXIiLCJyYXRpbmdDb3VudCIsInJhdGluZ1JldmlldyIsInVuZGVmaW5lZCIsImNhcmRTcGVjaWFsaXR5Iiwiam9pbiIsImJ1dHRvbkhvbGRlciIsImJ0blBob25lIiwiYnRuVmlkZW8iLCJidG5NZXNzYWdlIiwiaXRlbSIsImxhc3RfbmFtZSIsImZpcnN0X25hbWUiLCJjb21tZW50cyIsImJ1aWxkRG9tRWxlbWVudCIsInBhcmVudCIsInRlbXBsYXRlIiwiY2xhc3NOYW1lIiwiZWxlbSIsImRvY3VtZW50IiwiZGF0YSIsInJlcXVpcmUiLCJza2lwQnRuIiwicXVlcnlTZWxlY3RvciIsInNlYXJjaElucHV0IiwiZmlsdGVyRm9ybSIsImZpbHRlcklucHV0IiwiY2FyZEhvbGRlciIsImZpbHRlckJ0bkNvbnRhaW5lciIsIm1haW5EYXRhIiwiT2JqZWN0IiwiZGVmaW5lUHJvcGVydGllcyIsImdldCIsIiRkYXRhIiwic2V0IiwidmFsdWUiLCJyZW5kZXJFbGVtZW50cyIsIiRuYW1lRmlsdGVyVmFsdWUiLCIkc3BlY0ZpbHRlckFyciIsInJlbmRlckZpbHRlckJ1dHRvbnMiLCJhZGRTcGVjRmlsdGVyIiwiY29uc29sZSIsImxvZyIsIkFycmF5IiwiaXNBcnJheSIsInNwZWNGaWx0ZXJBcnIiLCJpbmNsdWRlcyIsInJlbW92ZVNwZWNGaWx0ZXIiLCJuZXdBcnIiLCJmaWx0ZXIiLCJlbCIsImNsZWFyRmlsdGVycyIsIm5hbWVGaWx0ZXJWYWx1ZSIsImFkZEV2ZW50TGlzdGVuZXIiLCJlIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsInRhcmdldCIsImFyciIsImlubmVySFRNTCIsImZvckVhY2giLCJidG4iLCJpbmNvbWluZ0RhdGEiLCJGaWx0ZXJNYW5hZ2VyIiwiZmlsdGVyQnlOYW1lIiwiZmlsdGVyQnlTcGVjIiwidXBkYXRlIiwicmVuZGVyIiwibWFwIiwiZnVsbE5hbWUiLCJ0b0xvd2VyQ2FzZSIsImluZGV4T2YiLCJmaWx0ZXJzIiwiZXZlcnkiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGtEQUEwQyxnQ0FBZ0M7QUFDMUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxnRUFBd0Qsa0JBQWtCO0FBQzFFO0FBQ0EseURBQWlELGNBQWM7QUFDL0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlEQUF5QyxpQ0FBaUM7QUFDMUUsd0hBQWdILG1CQUFtQixFQUFFO0FBQ3JJO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7OztBQUdBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFFTyxJQUFNQSxJQUFiO0FBQUE7QUFBQTtBQUNDLGtCQUFjO0FBQUE7O0FBQ2IsU0FBS0MsSUFBTCxHQUFZLEVBQVosQ0FEYSxDQUdiOztBQUNBLFNBQUtDLE1BQUwsR0FBYyxFQUFkO0FBQ0EsU0FBS0MsS0FBTCxHQUFhLEVBQWI7QUFDQSxTQUFLQyxNQUFMLEdBQWMsRUFBZDtBQUNBLFNBQUtDLElBQUwsR0FBWSxJQUFaO0FBQ0EsU0FBS0MsYUFBTCxHQUFxQixJQUFyQjtBQUNBLFNBQUtDLFVBQUwsR0FBa0IsRUFBbEI7QUFDQSxTQUFLQyxLQUFMLEdBQWEsRUFBYjtBQUNBLFNBQUtDLFdBQUwsR0FBbUIsRUFBbkI7QUFDQTs7QUFiRjtBQUFBO0FBQUEsc0NBZW1CO0FBQ2pCLFVBQUlSLElBQUksR0FBR1MsNkVBQWEsQ0FBQyxLQUFELEVBQVEsTUFBUixDQUF4QixDQURpQixDQUdqQjs7QUFDQSxVQUFJQyxTQUFTLEdBQUdELDZFQUFhLENBQUMsS0FBRCxFQUFRLFdBQVIsQ0FBN0I7QUFDQ0MsZUFBUyxDQUFDQyxZQUFWLENBQXVCLEtBQXZCLEVBQThCLEtBQUtWLE1BQW5DO0FBQ0FTLGVBQVMsQ0FBQ0MsWUFBVixDQUF1QixLQUF2QixFQUE4QixRQUE5QjtBQUNEWCxVQUFJLENBQUNZLFdBQUwsQ0FBaUJGLFNBQWpCLEVBUGlCLENBU2pCOztBQUNBLFVBQUlHLElBQUksR0FBR0osNkVBQWEsQ0FBQyxHQUFELEVBQU0saUNBQU4sQ0FBeEI7QUFDQ0ksVUFBSSxDQUFDRixZQUFMLENBQWtCLE1BQWxCLEVBQTBCLEdBQTFCO0FBQ0RYLFVBQUksQ0FBQ1ksV0FBTCxDQUFpQkMsSUFBakIsRUFaaUIsQ0FjakI7O0FBQ0EsVUFBSUMsU0FBUyxHQUFHTCw2RUFBYSxDQUFDLElBQUQsRUFBTyxhQUFQLENBQTdCO0FBQ0NLLGVBQVMsQ0FBQ0MsU0FBVixHQUFzQixLQUFLYixLQUEzQjtBQUNERixVQUFJLENBQUNZLFdBQUwsQ0FBaUJFLFNBQWpCLEVBakJpQixDQW1CakI7O0FBQ0EsVUFBSUUsVUFBVSxHQUFHUCw2RUFBYSxDQUFDLEtBQUQsRUFBUSxjQUFSLENBQTlCO0FBQ0NPLGdCQUFVLENBQUNELFNBQVgsR0FBdUIsS0FBS1osTUFBTCxHQUFjLFFBQWQsR0FBeUIsV0FBaEQ7QUFDQSxXQUFLQSxNQUFMLEdBQ0NhLFVBQVUsQ0FBQ0MsU0FBWCxDQUFxQkMsR0FBckIsQ0FBeUIsc0JBQXpCLENBREQsR0FFQ0YsVUFBVSxDQUFDQyxTQUFYLENBQXFCQyxHQUFyQixDQUF5Qix1QkFBekIsQ0FGRDtBQUdEbEIsVUFBSSxDQUFDWSxXQUFMLENBQWlCSSxVQUFqQixFQXpCaUIsQ0EyQmpCOztBQUNBLFVBQUlHLE1BQU0sR0FBR1YsNkVBQWEsQ0FBQyxLQUFELEVBQVEsUUFBUixDQUExQjtBQUNBLFVBQUlXLFdBQVcsR0FBR1gsNkVBQWEsQ0FBQyxLQUFELEVBQVEsZUFBUixDQUEvQjs7QUFDQSxXQUFLLElBQUlZLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsQ0FBcEIsRUFBdUJBLENBQUMsRUFBeEIsRUFBNEI7QUFDM0IsWUFBSUMsU0FBUyxHQUFHLEtBQUtsQixJQUFMLEdBQVlpQixDQUFaLEdBQWdCLGFBQWhCLEdBQWdDLGFBQWhEO0FBRUEsWUFBSUUsSUFBSSxHQUFHZCw2RUFBYSxDQUFDLEdBQUQsRUFBTWEsU0FBTixDQUF4QjtBQUNDRixtQkFBVyxDQUFDUixXQUFaLENBQXdCVyxJQUF4QjtBQUNEOztBQUNELFVBQUlDLFdBQVcsR0FBR2YsNkVBQWEsQ0FBQyxNQUFELEVBQVMsZUFBVCxDQUEvQjtBQUNDZSxpQkFBVyxDQUFDVCxTQUFaLGNBQTRCLEtBQUtYLElBQWpDO0FBQ0QsVUFBSXFCLFlBQVksR0FBR2hCLDZFQUFhLENBQUMsTUFBRCxFQUFTLGdCQUFULENBQWhDO0FBQ0NnQixrQkFBWSxDQUFDVixTQUFiLGFBQTRCLEtBQUtWLGFBQWpDO0FBQ0RjLFlBQU0sQ0FBQ1AsV0FBUCxDQUFtQlEsV0FBbkI7QUFDQUQsWUFBTSxDQUFDUCxXQUFQLENBQW1CWSxXQUFuQjtBQUNBTCxZQUFNLENBQUNQLFdBQVAsQ0FBbUJhLFlBQW5CO0FBQ0F6QixVQUFJLENBQUNZLFdBQUwsQ0FBaUJPLE1BQWpCLEVBM0NpQixDQThDakI7O0FBQ0EsVUFBSSxLQUFLYixVQUFMLEtBQW9Cb0IsU0FBeEIsRUFBbUM7QUFDbEMsWUFBSUMsY0FBYyxHQUFHbEIsNkVBQWEsQ0FBQyxLQUFELEVBQVEsa0JBQVIsQ0FBbEM7QUFDQ2tCLHNCQUFjLENBQUNaLFNBQWYsR0FBMkIsS0FBS1QsVUFBTCxDQUFnQnNCLElBQWhCLENBQXFCLElBQXJCLENBQTNCO0FBRUQ1QixZQUFJLENBQUNZLFdBQUwsQ0FBaUJlLGNBQWpCO0FBQ0EsT0FwRGdCLENBc0RqQjs7O0FBQ0EsVUFBSUUsWUFBWSxHQUFHcEIsNkVBQWEsQ0FBQyxLQUFELEVBQVEscUJBQVIsQ0FBaEM7QUFDQSxVQUFJcUIsUUFBUSxHQUFHckIsNkVBQWEsQ0FBQyxLQUFELEVBQVEsMkJBQVIsQ0FBNUI7QUFDQSxVQUFJc0IsUUFBUSxHQUFHdEIsNkVBQWEsQ0FBQyxLQUFELEVBQVEsMkJBQVIsQ0FBNUI7QUFDQSxVQUFJdUIsVUFBVSxHQUFHdkIsNkVBQWEsQ0FBQyxLQUFELEVBQVEsNkJBQVIsQ0FBOUI7QUFFQW9CLGtCQUFZLENBQUNqQixXQUFiLENBQXlCa0IsUUFBekI7QUFDQUQsa0JBQVksQ0FBQ2pCLFdBQWIsQ0FBeUJtQixRQUF6QjtBQUNBRixrQkFBWSxDQUFDakIsV0FBYixDQUF5Qm9CLFVBQXpCO0FBRUFoQyxVQUFJLENBQUNZLFdBQUwsQ0FBaUJpQixZQUFqQjtBQUVBLFdBQUs3QixJQUFMLEdBQVlBLElBQVo7QUFDQTtBQWxGRjtBQUFBO0FBQUEsMkJBb0ZRaUMsSUFwRlIsRUFvRmM7QUFDWixXQUFLaEMsTUFBTCxHQUFjZ0MsSUFBSSxDQUFDaEMsTUFBbkI7QUFDQSxXQUFLQyxLQUFMLEdBQWErQixJQUFJLENBQUNDLFNBQUwsR0FBaUJELElBQUksQ0FBQ0UsVUFBbkM7QUFDQSxXQUFLaEMsTUFBTCxHQUFjOEIsSUFBSSxDQUFDOUIsTUFBbkI7QUFDQSxXQUFLQyxJQUFMLEdBQVk2QixJQUFJLENBQUM3QixJQUFqQjtBQUNBLFdBQUtDLGFBQUwsR0FBcUI0QixJQUFJLENBQUNHLFFBQTFCO0FBQ0EsV0FBSzlCLFVBQUwsR0FBa0IyQixJQUFJLENBQUMzQixVQUFMLEtBQW9Cb0IsU0FBcEIsR0FBZ0NPLElBQUksQ0FBQzNCLFVBQXJDLEdBQWtELEVBQXBFO0FBQ0EsV0FBS0MsS0FBTCxHQUFhLFNBQWI7QUFDQSxXQUFLQyxXQUFMLEdBQW1CLFlBQW5CO0FBQ0EsV0FBSzZCLGVBQUw7QUFDQTtBQTlGRjtBQUFBO0FBQUEsMkJBZ0dRQyxNQWhHUixFQWdHZ0I7QUFDZEEsWUFBTSxDQUFDMUIsV0FBUCxDQUFtQixLQUFLWixJQUF4QjtBQUNBO0FBbEdGOztBQUFBO0FBQUEsSTs7Ozs7Ozs7Ozs7O0FDRkE7QUFBQTtBQUFPLFNBQVNTLGFBQVQsQ0FBdUI4QixRQUF2QixFQUFpQ0MsU0FBakMsRUFBNEM7QUFFbEQsTUFBSUMsSUFBSSxHQUFHQyxRQUFRLENBQUNqQyxhQUFULENBQXVCOEIsUUFBdkIsQ0FBWDtBQUVBRSxNQUFJLENBQUNELFNBQUwsR0FBaUJBLFNBQWpCO0FBRUEsU0FBT0MsSUFBUDtBQUNBLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1BEO0FBQ0E7QUFDQTs7QUFFQSxJQUFJRSxJQUFJLEdBQUdDLG1CQUFPLENBQUMsOENBQUQsQ0FBbEI7O0FBR0EsSUFBTUMsT0FBTyxHQUFHSCxRQUFRLENBQUNJLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBaEI7QUFDQSxJQUFNQyxXQUFXLEdBQUdMLFFBQVEsQ0FBQ0ksYUFBVCxDQUF1QixnQkFBdkIsQ0FBcEI7QUFDQSxJQUFNRSxVQUFVLEdBQUdOLFFBQVEsQ0FBQ0ksYUFBVCxDQUF1Qix1QkFBdkIsQ0FBbkI7QUFDQSxJQUFNRyxXQUFXLEdBQUdQLFFBQVEsQ0FBQ0ksYUFBVCxDQUF1QixnQkFBdkIsQ0FBcEI7QUFDQSxJQUFNSSxVQUFVLEdBQUdSLFFBQVEsQ0FBQ0ksYUFBVCxDQUF1QixjQUF2QixDQUFuQjtBQUNBLElBQU1LLGtCQUFrQixHQUFHVCxRQUFRLENBQUNJLGFBQVQsQ0FBdUIsMkJBQXZCLENBQTNCO0FBR0EsSUFBSU0sUUFBUSxHQUFHLEVBQWYsQyxDQUVBOztBQUNBQyxNQUFNLENBQUNDLGdCQUFQLENBQXdCRixRQUF4QixFQUFrQztBQUNqQyxVQUFRO0FBQ1BHLE9BQUcsRUFBRSxlQUFXO0FBQ2YsYUFBTyxLQUFLQyxLQUFaO0FBQ0EsS0FITTtBQUlQQyxPQUFHLEVBQUUsYUFBU0MsS0FBVCxFQUFnQjtBQUNwQixXQUFLRixLQUFMLEdBQWFFLEtBQWI7QUFDQUMsb0JBQWMsQ0FBQyxLQUFLSCxLQUFOLEVBQWEsS0FBS0ksZ0JBQWxCLEVBQW9DLEtBQUtDLGNBQXpDLENBQWQ7QUFDQTtBQVBNLEdBRHlCO0FBV2pDLHFCQUFtQjtBQUNsQk4sT0FBRyxFQUFFLGVBQVc7QUFDZixhQUFPLEtBQUtLLGdCQUFaO0FBQ0EsS0FIaUI7QUFJbEJILE9BQUcsRUFBRSxhQUFTQyxLQUFULEVBQWdCO0FBQ3BCLFdBQUtFLGdCQUFMLEdBQXdCRixLQUF4QjtBQUNBQyxvQkFBYyxDQUFDLEtBQUtILEtBQU4sRUFBYSxLQUFLSSxnQkFBbEIsRUFBb0MsS0FBS0MsY0FBekMsQ0FBZDtBQUNBO0FBUGlCLEdBWGM7QUFxQmpDLG1CQUFpQjtBQUNoQk4sT0FBRyxFQUFFLGVBQVc7QUFDZixhQUFPLEtBQUtNLGNBQVo7QUFDQSxLQUhlO0FBSWhCSixPQUFHLEVBQUUsYUFBU0MsS0FBVCxFQUFnQjtBQUNwQixXQUFLRyxjQUFMLEdBQXNCSCxLQUF0QjtBQUNBSSx5QkFBbUIsQ0FBQyxLQUFLRCxjQUFOLENBQW5CO0FBQ0FGLG9CQUFjLENBQUMsS0FBS0gsS0FBTixFQUFhLEtBQUtJLGdCQUFsQixFQUFvQyxLQUFLQyxjQUF6QyxDQUFkO0FBQ0E7QUFSZTtBQXJCZ0IsQ0FBbEMsRSxDQWlDQTs7QUFDQVQsUUFBUSxDQUFDVyxhQUFULEdBQXlCLFVBQVNMLEtBQVQsRUFBZ0I7QUFDeENNLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLFdBQVosRUFBeUIsSUFBekI7O0FBQ0EsTUFBSUMsS0FBSyxDQUFDQyxPQUFOLENBQWMsS0FBS0MsYUFBbkIsQ0FBSixFQUF1QztBQUN0QyxRQUFJLEtBQUtBLGFBQUwsQ0FBbUJDLFFBQW5CLENBQTRCWCxLQUE1QixDQUFKLEVBQXdDO0FBQ3ZDO0FBQ0E7O0FBQ0QsU0FBS1UsYUFBTCxjQUF5QkYsS0FBekIscUJBQWtDLEtBQUtFLGFBQXZDLFVBQXNEVixLQUF0RDtBQUNBLEdBTEQsTUFLTztBQUNOLFNBQUtVLGFBQUwsR0FBcUIsQ0FBQ1YsS0FBRCxDQUFyQjtBQUNBO0FBQ0QsQ0FWRCxDLENBWUE7OztBQUNBTixRQUFRLENBQUNrQixnQkFBVCxHQUE0QixVQUFTWixLQUFULEVBQWdCO0FBQzNDLE1BQUlhLE1BQU0sY0FBT0wsS0FBUCxxQkFBZ0IsS0FBS0UsYUFBckIsRUFBVjs7QUFDQUcsUUFBTSxHQUFHQSxNQUFNLENBQUNDLE1BQVAsQ0FBYyxVQUFBQyxFQUFFLEVBQUc7QUFDM0IsV0FBT0EsRUFBRSxJQUFJZixLQUFiO0FBQ0EsR0FGUSxDQUFUO0FBR0EsT0FBS1UsYUFBTCxHQUFxQkcsTUFBckI7QUFDQSxDQU5ELEMsQ0FRQTs7O0FBQ0FuQixRQUFRLENBQUNzQixZQUFULEdBQXdCLFlBQVc7QUFDbEMsT0FBS04sYUFBTCxHQUFxQixJQUFJRixLQUFKLEVBQXJCO0FBQ0EsT0FBS1MsZUFBTCxHQUF1QixFQUF2QjtBQUNBLENBSEQsQyxDQUtBOzs7QUFDQXZCLFFBQVEsQ0FBQ1QsSUFBVCxHQUFnQkEsSUFBaEIsQyxDQUlBOztBQUNBRSxPQUFPLENBQUMrQixnQkFBUixDQUF5QixPQUF6QixFQUFrQyxVQUFBQyxDQUFDLEVBQUk7QUFDdEM5QixhQUFXLENBQUNXLEtBQVosR0FBb0IsSUFBcEI7QUFBeUI7QUFDekJULGFBQVcsQ0FBQ1MsS0FBWixHQUFvQixJQUFwQjtBQUNBTixVQUFRLENBQUNzQixZQUFUO0FBRUEsQ0FMRCxFLENBT0E7O0FBQ0EzQixXQUFXLENBQUM2QixnQkFBWixDQUE2QixPQUE3QixFQUFzQyxVQUFTRSxLQUFULEVBQWdCO0FBQ3JEMUIsVUFBUSxDQUFDdUIsZUFBVCxHQUEyQixLQUFLakIsS0FBaEM7QUFDQSxDQUZELEUsQ0FJQTs7QUFDQVYsVUFBVSxDQUFDNEIsZ0JBQVgsQ0FBNEIsUUFBNUIsRUFBc0MsVUFBQ0MsQ0FBRCxFQUFPO0FBQzVDQSxHQUFDLENBQUNFLGNBQUY7QUFDQTNCLFVBQVEsQ0FBQ1csYUFBVCxDQUF1QmMsQ0FBQyxDQUFDRyxNQUFGLENBQVMsQ0FBVCxFQUFZdEIsS0FBbkM7QUFDQW1CLEdBQUMsQ0FBQ0csTUFBRixDQUFTLENBQVQsRUFBWXRCLEtBQVosR0FBb0IsSUFBcEI7QUFDQSxDQUpELEUsQ0FPQTtBQUVBOztBQUNBLFNBQVNJLG1CQUFULENBQTZCbUIsR0FBN0IsRUFBa0M7QUFDakM5QixvQkFBa0IsQ0FBQytCLFNBQW5CLEdBQStCLEVBQS9CO0FBRUFELEtBQUcsQ0FBQ0UsT0FBSixDQUFZLFVBQUFWLEVBQUUsRUFBRTtBQUNmLFFBQUlXLEdBQUcsR0FBRzNFLDZFQUFhLENBQUMsS0FBRCxFQUFRLGVBQVIsQ0FBdkI7QUFDQzJFLE9BQUcsQ0FBQ3JFLFNBQUosR0FBZ0IwRCxFQUFoQjtBQUNBVyxPQUFHLENBQUNSLGdCQUFKLENBQXFCLE9BQXJCLEVBQThCLFVBQUFDLENBQUMsRUFBSTtBQUNsQ3pCLGNBQVEsQ0FBQ2tCLGdCQUFULENBQTBCTyxDQUFDLENBQUNHLE1BQUYsQ0FBU2pFLFNBQW5DO0FBQ0EsS0FGRDtBQUdEb0Msc0JBQWtCLENBQUN2QyxXQUFuQixDQUErQndFLEdBQS9CO0FBQ0EsR0FQRDtBQVNBLEMsQ0FFRDs7O0FBQ0EsU0FBU3pCLGNBQVQsQ0FBd0IwQixZQUF4QixFQUFzQ1YsZUFBdEMsRUFBdURQLGFBQXZELEVBQXNFO0FBQ3JFbEIsWUFBVSxDQUFDZ0MsU0FBWCxHQUF1QixFQUF2QjtBQUNBLE1BQUl2QyxJQUFJLEdBQUcwQyxZQUFYOztBQUVBLE1BQUlWLGVBQWUsSUFBSWpELFNBQXZCLEVBQWtDO0FBQ2pDaUIsUUFBSSxHQUFHMkMsa0VBQWEsQ0FBQ0MsWUFBZCxDQUEyQjVDLElBQTNCLEVBQWlDZ0MsZUFBakMsQ0FBUDtBQUNBOztBQUVELE1BQUlQLGFBQWEsSUFBSTFDLFNBQXJCLEVBQWdDO0FBQy9CaUIsUUFBSSxHQUFHMkMsa0VBQWEsQ0FBQ0UsWUFBZCxDQUEyQjdDLElBQTNCLEVBQWlDeUIsYUFBakMsQ0FBUDtBQUNBOztBQUdEekIsTUFBSSxDQUFDd0MsT0FBTCxDQUFhLFVBQUFWLEVBQUUsRUFBSTtBQUNsQixRQUFJekUsSUFBSSxHQUFHLElBQUlELHFEQUFKLEVBQVg7QUFDQUMsUUFBSSxDQUFDeUYsTUFBTCxDQUFZaEIsRUFBWjtBQUNBekUsUUFBSSxDQUFDMEYsTUFBTCxDQUFZeEMsVUFBWjtBQUNBLEdBSkQ7QUFLQSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0lNLElBQU1vQyxhQUFiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUEsaUNBRXFCM0MsSUFGckIsRUFFMkJlLEtBRjNCLEVBRWtDO0FBQ2hDLGFBQU9mLElBQUksQ0FBQ2dELEdBQUwsQ0FBUyxVQUFBbEIsRUFBRSxFQUFJO0FBQ3BCLGlDQUFXQSxFQUFYO0FBQWVtQixrQkFBUSxFQUFFLENBQUNuQixFQUFFLENBQUN2QyxTQUFILEdBQWV1QyxFQUFFLENBQUN0QyxVQUFuQixFQUErQjBELFdBQS9CO0FBQXpCO0FBQ0EsT0FGSyxFQUVIckIsTUFGRyxDQUVJLFVBQUFDLEVBQUUsRUFBSTtBQUNmLGVBQU9BLEVBQUUsQ0FBQ21CLFFBQUgsQ0FBWUUsT0FBWixDQUFvQnBDLEtBQXBCLEtBQThCLENBQUMsQ0FBdEM7QUFDQSxPQUpLLENBQVA7QUFLQTtBQVJGO0FBQUE7QUFBQSxpQ0FVcUJmLElBVnJCLEVBVTJCb0QsT0FWM0IsRUFVb0M7QUFDbEMsYUFBT3BELElBQUksQ0FBQzZCLE1BQUwsQ0FBWSxVQUFBQyxFQUFFLEVBQUk7QUFDeEIsZUFBT3NCLE9BQU8sQ0FDWkosR0FESyxDQUNELFVBQUFsQixFQUFFO0FBQUEsaUJBQUlBLEVBQUUsQ0FBQ29CLFdBQUgsRUFBSjtBQUFBLFNBREQsRUFFTEcsS0FGSyxDQUVDLFVBQUEvRCxJQUFJLEVBQUk7QUFDZCxjQUFJd0MsRUFBRSxDQUFDbkUsVUFBSCxLQUFrQm9CLFNBQXRCLEVBQWlDO0FBQ2hDLG1CQUFPK0MsRUFBRSxDQUFDbkUsVUFBSCxDQUNMcUYsR0FESyxDQUNELFVBQUFsQixFQUFFO0FBQUEscUJBQUlBLEVBQUUsQ0FBQ29CLFdBQUgsRUFBSjtBQUFBLGFBREQsRUFFTHhCLFFBRkssQ0FFSXBDLElBRkosQ0FBUDtBQUdBO0FBQ0QsU0FSSyxDQUFQO0FBU0EsT0FWTSxDQUFQO0FBV0E7QUF0QkY7O0FBQUE7QUFBQSxJIiwiZmlsZSI6ImpzL21haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImltcG9ydCB7IGNyZWF0ZUVsZW1lbnQgfSBmcm9tICcuLi9oZWxwZXJzL0VsZW1lbnRDcmVhdG9yJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDYXJkIHtcclxuXHRjb25zdHJ1Y3RvcigpIHtcclxuXHRcdHRoaXMuY2FyZCA9IHt9O1xyXG5cclxuXHRcdC8vZWxlbWVudHMgZm9yIHVwZGF0ZVxyXG5cdFx0dGhpcy5hdmF0YXIgPSAnJztcclxuXHRcdHRoaXMudGl0bGUgPSAnJztcclxuXHRcdHRoaXMuc3RhdHVzID0gJyc7XHJcblx0XHR0aGlzLnJhdGUgPSBudWxsO1xyXG5cdFx0dGhpcy5jb21tZW50c0NvdW50ID0gbnVsbDtcclxuXHRcdHRoaXMuc3BlY2lhbGl0eSA9IFtdO1xyXG5cdFx0dGhpcy5wcmljZSA9ICcnO1xyXG5cdFx0dGhpcy5wcmljZVRpbWluZyA9ICcnO1xyXG5cdH1cclxuXHJcblx0YnVpbGREb21FbGVtZW50KCkge1xyXG5cdFx0bGV0IGNhcmQgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnY2FyZCcpO1xyXG5cclxuXHRcdC8vQXZhdGFyXHJcblx0XHRsZXQgYXZhdGFySW1nID0gY3JlYXRlRWxlbWVudCgnaW1nJywgJ2NhcmRfX2ltZycpO1xyXG5cdFx0XHRhdmF0YXJJbWcuc2V0QXR0cmlidXRlKCdzcmMnLCB0aGlzLmF2YXRhcik7XHJcblx0XHRcdGF2YXRhckltZy5zZXRBdHRyaWJ1dGUoJ2FsdCcsICdhdmF0YXInKTtcclxuXHRcdGNhcmQuYXBwZW5kQ2hpbGQoYXZhdGFySW1nKTtcclxuXHJcblx0XHQvL0xpbmtcclxuXHRcdGxldCBsaW5rID0gY3JlYXRlRWxlbWVudCgnYScsICdmYXMgZmEtY2hldnJvbi1yaWdodCBjYXJkX19tb3JlJyk7XHJcblx0XHRcdGxpbmsuc2V0QXR0cmlidXRlKCdocmVmJywgJyMnKVxyXG5cdFx0Y2FyZC5hcHBlbmRDaGlsZChsaW5rKVxyXG5cclxuXHRcdC8vQ2FyZFRpdGxlXHJcblx0XHRsZXQgY2FyZFRpdGxlID0gY3JlYXRlRWxlbWVudCgnaDMnLCAnY2FydF9fdGl0bGUnKTtcclxuXHRcdFx0Y2FyZFRpdGxlLmlubmVyVGV4dCA9IHRoaXMudGl0bGU7XHJcblx0XHRjYXJkLmFwcGVuZENoaWxkKGNhcmRUaXRsZSk7XHJcblxyXG5cdFx0Ly9DYXJkU3RhdHVzXHJcblx0XHRsZXQgY2FyZFN0YXR1cyA9IGNyZWF0ZUVsZW1lbnQoJ2RpdicsICdjYXJkX19zdGF0dXMnKTtcclxuXHRcdFx0Y2FyZFN0YXR1cy5pbm5lclRleHQgPSB0aGlzLnN0YXR1cyA/IFwi0JIg0YHQtdGC0LhcIiA6IFwi0J3QtSDQsiDRgdC10YLQuFwiO1xyXG5cdFx0XHR0aGlzLnN0YXR1cyA/IFxyXG5cdFx0XHRcdGNhcmRTdGF0dXMuY2xhc3NMaXN0LmFkZCgnY2FyZF9fc3RhdHVzLS1vbmxpbmUnKSA6XHJcblx0XHRcdFx0Y2FyZFN0YXR1cy5jbGFzc0xpc3QuYWRkKCdjYXJkX19zdGF0dXMtLW9mZmxpbmUnKTtcclxuXHRcdGNhcmQuYXBwZW5kQ2hpbGQoY2FyZFN0YXR1cyk7XHJcblxyXG5cdFx0Ly9SYXRpbmdcclxuXHRcdGxldCByYXRpbmcgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAncmF0aW5nJyk7XHJcblx0XHRsZXQgcmF0aW5nU3RhcnMgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAncmF0aW5nX19zdGFycycpO1xyXG5cdFx0Zm9yIChsZXQgaSA9IDA7IGkgPCA1OyBpKyspIHtcclxuXHRcdFx0bGV0IHN0YXJDbGFzcyA9IHRoaXMucmF0ZSA+IGkgPyAnZmFzIGZhLXN0YXInIDogJ2ZhciBmYS1zdGFyJztcclxuXHRcdFx0XHJcblx0XHRcdGxldCBzdGFyID0gY3JlYXRlRWxlbWVudCgnaScsIHN0YXJDbGFzcyk7XHJcblx0XHRcdFx0cmF0aW5nU3RhcnMuYXBwZW5kQ2hpbGQoc3RhcilcclxuXHRcdH1cclxuXHRcdGxldCByYXRpbmdDb3VudCA9IGNyZWF0ZUVsZW1lbnQoJ3NwYW4nLCAncmF0aW5nX19jb3VudCcpO1xyXG5cdFx0XHRyYXRpbmdDb3VudC5pbm5lclRleHQgPSBgKCR7dGhpcy5yYXRlfSlgO1xyXG5cdFx0bGV0IHJhdGluZ1JldmlldyA9IGNyZWF0ZUVsZW1lbnQoJ3NwYW4nLCAncmF0aW5nX19yZXZpZXcnKVxyXG5cdFx0XHRyYXRpbmdSZXZpZXcuaW5uZXJUZXh0ID0gYCR7dGhpcy5jb21tZW50c0NvdW50fSDQvtGC0LfRi9Cy0L7QsmBcclxuXHRcdHJhdGluZy5hcHBlbmRDaGlsZChyYXRpbmdTdGFycyk7XHJcblx0XHRyYXRpbmcuYXBwZW5kQ2hpbGQocmF0aW5nQ291bnQpO1xyXG5cdFx0cmF0aW5nLmFwcGVuZENoaWxkKHJhdGluZ1Jldmlldyk7XHJcblx0XHRjYXJkLmFwcGVuZENoaWxkKHJhdGluZyk7XHJcblxyXG5cdFx0XHJcblx0XHQvL0NhcmQgc3BlY2lhbGl0eSBpZiBleGlzdHNcclxuXHRcdGlmICh0aGlzLnNwZWNpYWxpdHkgIT09IHVuZGVmaW5lZCkge1xyXG5cdFx0XHRsZXQgY2FyZFNwZWNpYWxpdHkgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnY2FyZF9fc3BlY2lhbGl0eScpO1xyXG5cdFx0XHRcdGNhcmRTcGVjaWFsaXR5LmlubmVyVGV4dCA9IHRoaXMuc3BlY2lhbGl0eS5qb2luKCcsICcpO1xyXG5cclxuXHRcdFx0Y2FyZC5hcHBlbmRDaGlsZChjYXJkU3BlY2lhbGl0eSk7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC8vIEJ1dHRvbnNcclxuXHRcdGxldCBidXR0b25Ib2xkZXIgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnY2FyZF9fYnV0dG9uLWhvbGRlcicpO1xyXG5cdFx0bGV0IGJ0blBob25lID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2NhcmRfX2J1dHRvbiBidXR0b24tcGhvbmUnKVxyXG5cdFx0bGV0IGJ0blZpZGVvID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2NhcmRfX2J1dHRvbiBidXR0b24tdmlkZW8nKVxyXG5cdFx0bGV0IGJ0bk1lc3NhZ2UgPSBjcmVhdGVFbGVtZW50KCdkaXYnLCAnY2FyZF9fYnV0dG9uIGJ1dHRvbi1tZXNzYWdlJylcclxuXHJcblx0XHRidXR0b25Ib2xkZXIuYXBwZW5kQ2hpbGQoYnRuUGhvbmUpO1xyXG5cdFx0YnV0dG9uSG9sZGVyLmFwcGVuZENoaWxkKGJ0blZpZGVvKTtcclxuXHRcdGJ1dHRvbkhvbGRlci5hcHBlbmRDaGlsZChidG5NZXNzYWdlKTtcclxuXHRcdFxyXG5cdFx0Y2FyZC5hcHBlbmRDaGlsZChidXR0b25Ib2xkZXIpO1xyXG5cclxuXHRcdHRoaXMuY2FyZCA9IGNhcmQ7XHJcblx0fVxyXG5cclxuXHR1cGRhdGUoaXRlbSkge1xyXG5cdFx0dGhpcy5hdmF0YXIgPSBpdGVtLmF2YXRhcjtcclxuXHRcdHRoaXMudGl0bGUgPSBpdGVtLmxhc3RfbmFtZSArIGl0ZW0uZmlyc3RfbmFtZTtcclxuXHRcdHRoaXMuc3RhdHVzID0gaXRlbS5zdGF0dXM7XHJcblx0XHR0aGlzLnJhdGUgPSBpdGVtLnJhdGU7XHJcblx0XHR0aGlzLmNvbW1lbnRzQ291bnQgPSBpdGVtLmNvbW1lbnRzO1xyXG5cdFx0dGhpcy5zcGVjaWFsaXR5ID0gaXRlbS5zcGVjaWFsaXR5ICE9PSB1bmRlZmluZWQgPyBpdGVtLnNwZWNpYWxpdHkgOiBbXTtcclxuXHRcdHRoaXMucHJpY2UgPSBcIjI1MCDQs9GA0L1cIjtcclxuXHRcdHRoaXMucHJpY2VUaW1pbmcgPSBcIigxMCDQvNC40L3Rg9GCKVwiO1xyXG5cdFx0dGhpcy5idWlsZERvbUVsZW1lbnQoKTtcclxuXHR9XHJcblxyXG5cdHJlbmRlcihwYXJlbnQpIHtcclxuXHRcdHBhcmVudC5hcHBlbmRDaGlsZCh0aGlzLmNhcmQpO1xyXG5cdH1cclxufSIsImV4cG9ydCBmdW5jdGlvbiBjcmVhdGVFbGVtZW50KHRlbXBsYXRlLCBjbGFzc05hbWUpIHtcclxuXHJcblx0bGV0IGVsZW0gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KHRlbXBsYXRlKTtcclxuXHJcblx0ZWxlbS5jbGFzc05hbWUgPSBjbGFzc05hbWU7XHJcblxyXG5cdHJldHVybiBlbGVtO1xyXG59XHJcblxyXG4iLCJpbXBvcnQgeyBDYXJkIH0gZnJvbSAnLi9jb21wb25lbnRzL0NhcmQnO1xyXG5pbXBvcnQgeyBjcmVhdGVFbGVtZW50IH0gZnJvbSAnLi9oZWxwZXJzL0VsZW1lbnRDcmVhdG9yJztcclxuaW1wb3J0IHsgRmlsdGVyTWFuYWdlciB9IGZyb20gJy4vdXRpbHMvRmlsdGVyTWFuYWdlcidcclxuXHJcbmxldCBkYXRhID0gcmVxdWlyZSgnLi91c2VyX2RhdGEuanNvbicpO1xyXG5cclxuXHJcbmNvbnN0IHNraXBCdG4gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuc2tpcCcpO1xyXG5jb25zdCBzZWFyY2hJbnB1dCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zZWFyY2hfX2lucHV0Jyk7XHJcbmNvbnN0IGZpbHRlckZvcm0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmZpbHRlcl9faW5wdXQtaG9sZGVyXCIpO1xyXG5jb25zdCBmaWx0ZXJJbnB1dCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5maWx0ZXJfX2lucHV0Jyk7XHJcbmNvbnN0IGNhcmRIb2xkZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY2FyZC1ob2xkZXInKTtcclxuY29uc3QgZmlsdGVyQnRuQ29udGFpbmVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmZpbHRlcl9fYnV0dG9uLWNvbnRhaW5lcicpO1xyXG5cclxuXHJcbmxldCBtYWluRGF0YSA9IHt9O1xyXG5cclxuLy9BZGQgcmVhY3Rpdml0eSB0byBtYWluRGF0YSBvYmplY3RcclxuT2JqZWN0LmRlZmluZVByb3BlcnRpZXMobWFpbkRhdGEsIHtcclxuXHQnZGF0YSc6IHtcclxuXHRcdGdldDogZnVuY3Rpb24oKSB7XHJcblx0XHRcdHJldHVybiB0aGlzLiRkYXRhO1xyXG5cdFx0fSxcclxuXHRcdHNldDogZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdFx0dGhpcy4kZGF0YSA9IHZhbHVlO1xyXG5cdFx0XHRyZW5kZXJFbGVtZW50cyh0aGlzLiRkYXRhLCB0aGlzLiRuYW1lRmlsdGVyVmFsdWUsIHRoaXMuJHNwZWNGaWx0ZXJBcnIpO1xyXG5cdFx0fVxyXG5cdH0sXHJcblxyXG5cdCduYW1lRmlsdGVyVmFsdWUnOiB7XHJcblx0XHRnZXQ6IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRyZXR1cm4gdGhpcy4kbmFtZUZpbHRlclZhbHVlO1xyXG5cdFx0fSxcclxuXHRcdHNldDogZnVuY3Rpb24odmFsdWUpIHtcclxuXHRcdFx0dGhpcy4kbmFtZUZpbHRlclZhbHVlID0gdmFsdWU7XHJcblx0XHRcdHJlbmRlckVsZW1lbnRzKHRoaXMuJGRhdGEsIHRoaXMuJG5hbWVGaWx0ZXJWYWx1ZSwgdGhpcy4kc3BlY0ZpbHRlckFycik7XHJcblx0XHR9XHJcblx0fSxcclxuXHJcblx0J3NwZWNGaWx0ZXJBcnInOiB7XHJcblx0XHRnZXQ6IGZ1bmN0aW9uKCkge1xyXG5cdFx0XHRyZXR1cm4gdGhpcy4kc3BlY0ZpbHRlckFycjtcclxuXHRcdH0sXHJcblx0XHRzZXQ6IGZ1bmN0aW9uKHZhbHVlKSB7XHJcblx0XHRcdHRoaXMuJHNwZWNGaWx0ZXJBcnIgPSB2YWx1ZTtcclxuXHRcdFx0cmVuZGVyRmlsdGVyQnV0dG9ucyh0aGlzLiRzcGVjRmlsdGVyQXJyKTtcclxuXHRcdFx0cmVuZGVyRWxlbWVudHModGhpcy4kZGF0YSwgdGhpcy4kbmFtZUZpbHRlclZhbHVlLCB0aGlzLiRzcGVjRmlsdGVyQXJyKTtcclxuXHRcdH1cclxuXHR9XHJcbn0pXHJcblxyXG4vL01ldGhvZCBmb3IgYWRkaW5nIHNwZWNpYWxpdHkgZmlsdGVyXHJcbm1haW5EYXRhLmFkZFNwZWNGaWx0ZXIgPSBmdW5jdGlvbih2YWx1ZSkge1xyXG5cdGNvbnNvbGUubG9nKCdhZGRGaWx0ZXInLCB0aGlzKVxyXG5cdGlmIChBcnJheS5pc0FycmF5KHRoaXMuc3BlY0ZpbHRlckFycikpIHtcclxuXHRcdGlmICh0aGlzLnNwZWNGaWx0ZXJBcnIuaW5jbHVkZXModmFsdWUpKSB7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHRcdHRoaXMuc3BlY0ZpbHRlckFyciA9IG5ldyBBcnJheSguLi50aGlzLnNwZWNGaWx0ZXJBcnIsIHZhbHVlKVxyXG5cdH0gZWxzZSB7XHJcblx0XHR0aGlzLnNwZWNGaWx0ZXJBcnIgPSBbdmFsdWVdXHJcblx0fVxyXG59XHJcblxyXG4vL01ldGhvZCBmb3IgcmVtb3Zpbmcgc2VwYXJhdGUgc3BlY2lhbGl0eSBmaWx0ZXJcclxubWFpbkRhdGEucmVtb3ZlU3BlY0ZpbHRlciA9IGZ1bmN0aW9uKHZhbHVlKSB7XHJcblx0bGV0IG5ld0FyciA9IG5ldyBBcnJheSguLi50aGlzLnNwZWNGaWx0ZXJBcnIpO1xyXG5cdG5ld0FyciA9IG5ld0Fyci5maWx0ZXIoZWw9PiB7XHJcblx0XHRyZXR1cm4gZWwgIT0gdmFsdWU7XHJcblx0fSlcclxuXHR0aGlzLnNwZWNGaWx0ZXJBcnIgPSBuZXdBcnI7XHJcbn1cclxuXHJcbi8vTWV0aG9kIGZvciByZW1vdmluZyBhbGwgZmlsdGVyc1xyXG5tYWluRGF0YS5jbGVhckZpbHRlcnMgPSBmdW5jdGlvbigpIHtcclxuXHR0aGlzLnNwZWNGaWx0ZXJBcnIgPSBuZXcgQXJyYXkoKTtcclxuXHR0aGlzLm5hbWVGaWx0ZXJWYWx1ZSA9ICcnO1xyXG59XHJcblxyXG4vL0luZmxhdGUgcmVhY3RpdmUgb2JqZWN0IHdpdGggZGF0YVxyXG5tYWluRGF0YS5kYXRhID0gZGF0YTtcclxuXHJcblxyXG5cclxuLy9MaXN0ZW4gZm9yIFNLSVAgYnV0dG9uIGNsaWNrXHJcbnNraXBCdG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBlID0+IHtcclxuXHRzZWFyY2hJbnB1dC52YWx1ZSA9IG51bGw7O1xyXG5cdGZpbHRlcklucHV0LnZhbHVlID0gbnVsbDtcclxuXHRtYWluRGF0YS5jbGVhckZpbHRlcnMoKTtcclxuXHRcclxufSlcclxuXHJcbi8vTGlzdGVuIGZvciBzZWFyY2hJbnB1dCB0ZXh0IGNoYW5nZVxyXG5zZWFyY2hJbnB1dC5hZGRFdmVudExpc3RlbmVyKCdpbnB1dCcsIGZ1bmN0aW9uKGV2ZW50KSB7XHJcblx0bWFpbkRhdGEubmFtZUZpbHRlclZhbHVlID0gdGhpcy52YWx1ZTtcclxufSlcclxuXHJcbi8vTGlzdGVuIGZvciBzdWJtaXQgb2YgZmlsdGVyIGZvcm0gKFwiUHJlc3MgRW50ZXJcIilcclxuZmlsdGVyRm9ybS5hZGRFdmVudExpc3RlbmVyKCdzdWJtaXQnLCAoZSkgPT4ge1xyXG5cdGUucHJldmVudERlZmF1bHQoKTtcclxuXHRtYWluRGF0YS5hZGRTcGVjRmlsdGVyKGUudGFyZ2V0WzBdLnZhbHVlKTtcclxuXHRlLnRhcmdldFswXS52YWx1ZSA9IG51bGw7XHJcbn0pXHJcblxyXG5cclxuLy8gPT09PT09PT09PT09PT09PT0gUkVOREVSID09PT09PT09PT09PT09PT09PVxyXG5cclxuLy9SZW5kZXIgZmlsdGVyIGJ1dHRvbnNcclxuZnVuY3Rpb24gcmVuZGVyRmlsdGVyQnV0dG9ucyhhcnIpIHtcclxuXHRmaWx0ZXJCdG5Db250YWluZXIuaW5uZXJIVE1MID0gJyc7XHJcblxyXG5cdGFyci5mb3JFYWNoKGVsPT57XHJcblx0XHRsZXQgYnRuID0gY3JlYXRlRWxlbWVudCgnZGl2JywgJ2ZpbHRlci1idXR0b24nKVxyXG5cdFx0XHRidG4uaW5uZXJUZXh0ID0gZWw7XHJcblx0XHRcdGJ0bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGUgPT4ge1xyXG5cdFx0XHRcdG1haW5EYXRhLnJlbW92ZVNwZWNGaWx0ZXIoZS50YXJnZXQuaW5uZXJUZXh0KVxyXG5cdFx0XHR9KVxyXG5cdFx0ZmlsdGVyQnRuQ29udGFpbmVyLmFwcGVuZENoaWxkKGJ0bik7XHJcblx0fSlcclxuXHRcclxufVxyXG5cclxuLy9SZW5kZXIgY2FyZHNcclxuZnVuY3Rpb24gcmVuZGVyRWxlbWVudHMoaW5jb21pbmdEYXRhLCBuYW1lRmlsdGVyVmFsdWUsIHNwZWNGaWx0ZXJBcnIpIHtcclxuXHRjYXJkSG9sZGVyLmlubmVySFRNTCA9IFwiXCI7XHJcblx0bGV0IGRhdGEgPSBpbmNvbWluZ0RhdGE7XHJcblxyXG5cdGlmIChuYW1lRmlsdGVyVmFsdWUgIT0gdW5kZWZpbmVkKSB7XHJcblx0XHRkYXRhID0gRmlsdGVyTWFuYWdlci5maWx0ZXJCeU5hbWUoZGF0YSwgbmFtZUZpbHRlclZhbHVlKVxyXG5cdH1cclxuXHJcblx0aWYgKHNwZWNGaWx0ZXJBcnIgIT0gdW5kZWZpbmVkKSB7XHJcblx0XHRkYXRhID0gRmlsdGVyTWFuYWdlci5maWx0ZXJCeVNwZWMoZGF0YSwgc3BlY0ZpbHRlckFycilcclxuXHR9XHJcblxyXG5cclxuXHRkYXRhLmZvckVhY2goZWwgPT4ge1xyXG5cdFx0bGV0IGNhcmQgPSBuZXcgQ2FyZCgpO1xyXG5cdFx0Y2FyZC51cGRhdGUoZWwpO1xyXG5cdFx0Y2FyZC5yZW5kZXIoY2FyZEhvbGRlcik7XHJcblx0fSk7XHJcbn0iLCJleHBvcnQgY2xhc3MgRmlsdGVyTWFuYWdlciB7XHJcblxyXG5cdHN0YXRpYyBmaWx0ZXJCeU5hbWUoZGF0YSwgdmFsdWUpIHtcclxuXHRcdHJldHVybiBkYXRhLm1hcChlbCA9PiB7XHJcblx0XHRcdFx0cmV0dXJuIHsuLi5lbCwgZnVsbE5hbWU6IChlbC5sYXN0X25hbWUgKyBlbC5maXJzdF9uYW1lKS50b0xvd2VyQ2FzZSgpfVxyXG5cdFx0XHR9KS5maWx0ZXIoZWwgPT4ge1xyXG5cdFx0XHRcdHJldHVybiBlbC5mdWxsTmFtZS5pbmRleE9mKHZhbHVlKSAhPSAtMTtcclxuXHRcdFx0fSk7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgZmlsdGVyQnlTcGVjKGRhdGEsIGZpbHRlcnMpIHtcclxuXHRcdHJldHVybiBkYXRhLmZpbHRlcihlbCA9PiB7XHJcblx0XHRcdHJldHVybiBmaWx0ZXJzXHJcblx0XHRcdFx0Lm1hcChlbCA9PiBlbC50b0xvd2VyQ2FzZSgpKVxyXG5cdFx0XHRcdC5ldmVyeShpdGVtID0+IHtcclxuXHRcdFx0XHRcdGlmIChlbC5zcGVjaWFsaXR5ICE9PSB1bmRlZmluZWQpIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIGVsLnNwZWNpYWxpdHlcclxuXHRcdFx0XHRcdFx0XHQubWFwKGVsID0+IGVsLnRvTG93ZXJDYXNlKCkpXHJcblx0XHRcdFx0XHRcdFx0LmluY2x1ZGVzKGl0ZW0pXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSlcclxuXHRcdH0pXHJcblx0fVxyXG59Il0sInNvdXJjZVJvb3QiOiIifQ==