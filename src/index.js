import { Card } from './components/Card';
import { createElement } from './helpers/ElementCreator';
import { FilterManager } from './utils/FilterManager'

let data = require('./user_data.json');


const skipBtn = document.querySelector('.skip');
const searchInput = document.querySelector('.search__input');
const filterForm = document.querySelector(".filter__input-holder");
const filterInput = document.querySelector('.filter__input');
const cardHolder = document.querySelector('.card-holder');
const filterBtnContainer = document.querySelector('.filter__button-container');


let mainData = {};

//Add reactivity to mainData object
Object.defineProperties(mainData, {
	'data': {
		get: function() {
			return this.$data;
		},
		set: function(value) {
			this.$data = value;
			renderElements(this.$data, this.$nameFilterValue, this.$specFilterArr);
		}
	},

	'nameFilterValue': {
		get: function() {
			return this.$nameFilterValue;
		},
		set: function(value) {
			this.$nameFilterValue = value;
			renderElements(this.$data, this.$nameFilterValue, this.$specFilterArr);
		}
	},

	'specFilterArr': {
		get: function() {
			return this.$specFilterArr;
		},
		set: function(value) {
			this.$specFilterArr = value;
			renderFilterButtons(this.$specFilterArr);
			renderElements(this.$data, this.$nameFilterValue, this.$specFilterArr);
		}
	}
})

//Method for adding speciality filter
mainData.addSpecFilter = function(value) {
	console.log('addFilter', this)
	if (Array.isArray(this.specFilterArr)) {
		if (this.specFilterArr.includes(value)) {
			return;
		}
		this.specFilterArr = new Array(...this.specFilterArr, value)
	} else {
		this.specFilterArr = [value]
	}
}

//Method for removing separate speciality filter
mainData.removeSpecFilter = function(value) {
	let newArr = new Array(...this.specFilterArr);
	newArr = newArr.filter(el=> {
		return el != value;
	})
	this.specFilterArr = newArr;
}

//Method for removing all filters
mainData.clearFilters = function() {
	this.specFilterArr = new Array();
	this.nameFilterValue = '';
}

//Inflate reactive object with data
mainData.data = data;



//Listen for SKIP button click
skipBtn.addEventListener('click', e => {
	searchInput.value = null;;
	filterInput.value = null;
	mainData.clearFilters();
	
})

//Listen for searchInput text change
searchInput.addEventListener('input', function(event) {
	mainData.nameFilterValue = this.value;
})

//Listen for submit of filter form ("Press Enter")
filterForm.addEventListener('submit', (e) => {
	e.preventDefault();
	mainData.addSpecFilter(e.target[0].value);
	e.target[0].value = null;
})


// ================= RENDER ==================

//Render filter buttons
function renderFilterButtons(arr) {
	filterBtnContainer.innerHTML = '';

	arr.forEach(el=>{
		let btn = createElement('div', 'filter-button')
			btn.innerText = el;
			btn.addEventListener('click', e => {
				mainData.removeSpecFilter(e.target.innerText)
			})
		filterBtnContainer.appendChild(btn);
	})
	
}

//Render cards
function renderElements(incomingData, nameFilterValue, specFilterArr) {
	cardHolder.innerHTML = "";
	let data = incomingData;

	if (nameFilterValue != undefined) {
		data = FilterManager.filterByName(data, nameFilterValue)
	}

	if (specFilterArr != undefined) {
		data = FilterManager.filterBySpec(data, specFilterArr)
	}


	data.forEach(el => {
		let card = new Card();
		card.update(el);
		card.render(cardHolder);
	});
}