export function createElement(template, className) {

	let elem = document.createElement(template);

	elem.className = className;

	return elem;
}

