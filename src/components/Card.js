import { createElement } from '../helpers/ElementCreator';

export class Card {
	constructor() {
		this.card = {};

		//elements for update
		this.avatar = '';
		this.title = '';
		this.status = '';
		this.rate = null;
		this.commentsCount = null;
		this.speciality = [];
		this.price = '';
		this.priceTiming = '';
	}

	buildDomElement() {
		let card = createElement('div', 'card');

		//Avatar
		let avatarImg = createElement('img', 'card__img');
			avatarImg.setAttribute('src', this.avatar);
			avatarImg.setAttribute('alt', 'avatar');
		card.appendChild(avatarImg);

		//Link
		let link = createElement('a', 'fas fa-chevron-right card__more');
			link.setAttribute('href', '#')
		card.appendChild(link)

		//CardTitle
		let cardTitle = createElement('h3', 'cart__title');
			cardTitle.innerText = this.title;
		card.appendChild(cardTitle);

		//CardStatus
		let cardStatus = createElement('div', 'card__status');
			cardStatus.innerText = this.status ? "В сети" : "Не в сети";
			this.status ? 
				cardStatus.classList.add('card__status--online') :
				cardStatus.classList.add('card__status--offline');
		card.appendChild(cardStatus);

		//Rating
		let rating = createElement('div', 'rating');
		let ratingStars = createElement('div', 'rating__stars');
		for (let i = 0; i < 5; i++) {
			let starClass = this.rate > i ? 'fas fa-star' : 'far fa-star';
			
			let star = createElement('i', starClass);
				ratingStars.appendChild(star)
		}
		let ratingCount = createElement('span', 'rating__count');
			ratingCount.innerText = `(${this.rate})`;
		let ratingReview = createElement('span', 'rating__review')
			ratingReview.innerText = `${this.commentsCount} отзывов`
		rating.appendChild(ratingStars);
		rating.appendChild(ratingCount);
		rating.appendChild(ratingReview);
		card.appendChild(rating);

		
		//Card speciality if exists
		if (this.speciality !== undefined) {
			let cardSpeciality = createElement('div', 'card__speciality');
				cardSpeciality.innerText = this.speciality.join(', ');

			card.appendChild(cardSpeciality);
		}
		
		// Buttons
		let buttonHolder = createElement('div', 'card__button-holder');
		let btnPhone = createElement('div', 'card__button button-phone')
		let btnVideo = createElement('div', 'card__button button-video')
		let btnMessage = createElement('div', 'card__button button-message')

		buttonHolder.appendChild(btnPhone);
		buttonHolder.appendChild(btnVideo);
		buttonHolder.appendChild(btnMessage);
		
		card.appendChild(buttonHolder);

		this.card = card;
	}

	update(item) {
		this.avatar = item.avatar;
		this.title = item.last_name + item.first_name;
		this.status = item.status;
		this.rate = item.rate;
		this.commentsCount = item.comments;
		this.speciality = item.speciality !== undefined ? item.speciality : [];
		this.price = "250 грн";
		this.priceTiming = "(10 минут)";
		this.buildDomElement();
	}

	render(parent) {
		parent.appendChild(this.card);
	}
}