export class FilterManager {

	static filterByName(data, value) {
		return data.map(el => {
				return {...el, fullName: (el.last_name + el.first_name).toLowerCase()}
			}).filter(el => {
				return el.fullName.indexOf(value) != -1;
			});
	}

	static filterBySpec(data, filters) {
		return data.filter(el => {
			return filters
				.map(el => el.toLowerCase())
				.every(item => {
					if (el.speciality !== undefined) {
						return el.speciality
							.map(el => el.toLowerCase())
							.includes(item)
					}
				})
		})
	}
}